import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.Scanner;

public class MethodesAdmin {

	Connection co;
	Scanner scanner;
	
	public MethodesAdmin() {
		
		//se connecter a la db
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Driver PostgreSQL manquant !");
				System.exit(1);
			}
			
			//String url = "jdbc:postgresql://localhost:5432/dbprojet";
			String url="jdbc:postgresql://172.24.2.6:5432/dbsimonpenelle";
			Connection conn=null;
			
			try {
				conn=DriverManager.getConnection(url,"simonpenelle","F43T1HPJC");
				//conn=DriverManager.getConnection(url,"postgres","chocotigri");
			} catch (SQLException e) {
				
				e.printStackTrace();
				System.out.println("Impossible de joindre le server !");
				System.exit(1);
			}
		this.co = conn;
		 
		
		this.scanner = new Scanner(System.in);
		
	}
	/*methode qui affiche le menu initial
	 * @return le choix de l'utilisateur
	 */
	public int afficherMenu() {
		System.out.println();
		System.out.println("quelle fonctionalit� souhaitez vous utiliser ('-1' pour quitter)");
		System.out.println("1 - ajout salle");
		System.out.println("2 - ajout artiste");
		System.out.println("3 - ajout evenement");
		System.out.println("4 - ajout festival");
		System.out.println("5 - ajout concert");
		System.out.println("6 - visualiser aristes");
		System.out.println("7 - visualiser concerts entre 2 dates");
		
		
		String repons = scanner.nextLine();
		int reponse = Integer.parseInt(repons);
		
		return reponse;
	}
	
	
	/*methode pour ajouter une salle
	 * @param connection a la DB
	 * @param nom de la salle
	 * @param ville
	 * @param capacite max de la salle
	 */
	public void ajouterSalle() {
		
		System.out.println("entrez le nom de la salle");
		String nom = scanner.nextLine();
		
		System.out.println("entrez la ville");
		String ville = scanner.nextLine();
		
		System.out.println("entrez la capacit� de la salle");
		String capacitey = scanner.nextLine();
		int capacite = Integer.parseInt(capacitey);
		
		try {
			PreparedStatement ps = co.prepareStatement("SELECT projet.ajoutSalle(?,?,?);");
			ps.setString(1,nom);
			ps.setString(2,ville);
			ps.setInt(3,capacite);
			ps.executeQuery();
			System.out.println("la salle a bien �t� ajout�e");
		} 
		catch (SQLException se) {
			System.out.println("Erreur lors de l�insertion !");
			System.out.println(se.getMessage());
		}
		
		
	}
	
	/*methode pour ajouter un artiste
	 * @param connection a la DB
	 * @param nom de l'artiste
	 * @param nationalit�
	 */
	public  void ajouterArtiste() {
		
		System.out.println("entrez le nom de l'artiste");
		String nom = scanner.nextLine();
		
		System.out.println("entrez sa nationalit�");
		String nationalite = scanner.nextLine();
		
		try {
			PreparedStatement ps = co.prepareStatement("SELECT projet.ajoutArtiste(?,?);");
			ps.setString(1,nom);
			ps.setString(2,nationalite);
			ps.executeQuery();
			System.out.println("l'artiste a bien �t� ajout�e");
		} 
		catch (SQLException se) {
			System.out.println("Erreur lors de l�insertion !");
			System.out.println(se.getMessage());
		}
	}
	

	
	/*methode pour ajouter un evenent
	 * @param connection a la DB
	 * @param nom de l'evenemnt
	 * @param date de l'event
	 * @param id de la salle assosi� (TODO)
	 * @param prix en � (entier)
	 * @param id du festival assosi� (facultatif) (TODO)
	 */
	public  void ajouterEvenement() {
		
		System.out.println("entrez le nom de l'evenement");
		String nom = scanner.nextLine();
		
		System.out.println("entrez la date(ex. 2019-11-21)");
		String strDate = scanner.nextLine();
		Date date = Date.valueOf(strDate);
		
		System.out.println("entrez le prix (entier)");
		String prixy = scanner.nextLine();
		int prix = Integer.parseInt(prixy);
		
		System.out.println("veuillez s�lectionner une salle pour l'evenement...(obligatoire)");
		visualiserSalles();
		String salley = scanner.nextLine();
		int salle = Integer.parseInt(salley);
		
		System.out.println("veuillez s�lectionner un festival (facultatif) ou tapez '-1'");
		visualiserFestivals();
		String festivaly = scanner.nextLine();
		int festival = Integer.parseInt(festivaly);
		
	
		try {
			PreparedStatement ps = co.prepareStatement("SELECT projet.ajoutEvenmt(?,?,?,?,?);");
			ps.setString(1,nom);
			ps.setDate(2,date);
			ps.setInt(3,salle);
			ps.setInt(4,prix);
			ps.setInt(5,festival);
			ps.executeQuery();
			System.out.println("l'evenement a bien �t� ajout�e");
		} 
		catch (SQLException se) {
			System.out.println("Erreur lors de l�insertion !");
			System.out.println(se.getMessage());
		}
	}
	
	
	/*methode pour ajouter un festival
	 * @param connection a la DB
	 * @param nom du festival
	 */
	public void ajouterFestival() {
		
		System.out.println("entrez le nom du festival");
		String nom = scanner.nextLine();
		
		try {
			PreparedStatement ps = co.prepareStatement("SELECT projet.ajoutFestival(?);");
			ps.setString(1,nom);
		
			ps.executeQuery();
			System.out.println("le festival a bien �t� ajout�e");
		} 
		catch (SQLException se) {
			System.out.println("Erreur lors de l�insertion !");
			System.out.println(se.getMessage());
		}
	}

	
	/*methode pour ajouter un concert
	 * @param connection a la DB
	 * @param id de l'evenement assosi� (TODO)
	 * @param id de l'artiste assossi�
	 * @param heure de d�but 
	 */
	public void ajouterConcert() {
		
		System.out.println("selectionez un evenement ");
		System.out.println("num |  nom  |  date  | salle");
		this.visualiserEvent();
		String evenementy = scanner.nextLine();
		int evenement =  Integer.parseInt(evenementy);
		
		System.out.println("selectionez le num de l'artiste qui participe");
		this.visualiserArtistes();
		String artistey = scanner.nextLine();
		int artiste =  Integer.parseInt(artistey);
		
		System.out.println("entrez l'heure de d�but (ex. 22:00)");
		String strHoraire = scanner.nextLine();
		strHoraire+=":00";
		Time horaire =Time.valueOf(strHoraire);
		
		
		try {
			PreparedStatement ps = co.prepareStatement("SELECT projet.ajoutConcert(?,?,?);");
			ps.setTime(1,horaire);
			ps.setInt(2,artiste);
			ps.setInt(3,evenement);
			
			ps.executeQuery();
			System.out.println("le concert a bien �t� ajout�e");
			
			
		} 
		catch (SQLException se) {
			System.out.println("Erreur lors de l�insertion !");
			System.out.println(se.getMessage());
		}
		
	}
	
	
	/*methode pour visualiser la liste des artistes
	 * 
	 */
	public void visualiserArtistes() {
		
		try {
			Statement s = co.createStatement();
			
			try(ResultSet rs= s.executeQuery("SELECT * FROM projet.listeArtiste;")){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | "+rs.getString(2)+" | "+rs.getString(3)+" | ticket vendus: "+rs.getString(4));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage());
		}
	}
	
	/*methode pour visualiser la liste des evenements
	 * et concerts entre 2 dates donn�es
	 * @param date1: date de d�but de la recherche
	 * @param date2: date de fin de la recherche
	 */
	public  void visualiserEventConcertsDate() {
		
		System.out.println("entrez la date de debut (ex.2020-12-31)");
		String strDate1 = scanner.nextLine();
		Date date1 =Date.valueOf(strDate1);
		
		System.out.println("entrez la date de fin");
		String strDate2 = scanner.nextLine();
		Date date2 =Date.valueOf(strDate2);		
		
		try {
			
			PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.listeEvenementss WHERE date_evenement >= ? AND date_evenement <=?;");
			ps.setDate(1,date1);
			ps.setDate(2,date2);
			
			try(ResultSet rs= ps.executeQuery()){
				
				while(rs.next()) {
					System.out.println("nom_event: "+rs.getString(1)+" | date: "+rs.getString(2)+" | nom_salle : "+rs.getString(3)+" | nom_fest: "+rs.getString(4)+" | nbr_ticket: "+rs.getString(5));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage());
			 
		}
	}
	
	
	//methode pour visualiser les event
	public  void visualiserEvent() {		
		
		try {
			
			PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.listeEvenements;");		
			try(ResultSet rs= ps.executeQuery()){
				
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | "+rs.getString(2)+" | "+rs.getString(3)+" | "+rs.getString(4));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage());
			
		}
	}
	
	
	//methode pour visualiser les salles
	public  void visualiserSalles() {		
			
			try {
				
				PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.listeSalles;");		
				try(ResultSet rs= ps.executeQuery()){
					
					while(rs.next()) {
						System.out.println(rs.getString(1)+" | "+rs.getString(2));
					}
				}
			} 
			catch (SQLException se) { 
				System.out.println(se.getMessage());
			}
	}
	
	public  void visualiserFestivals() {		
		
		try {
			System.out.println();
			if(this.CompterFestivals()==0)System.out.println("[ Il n'y a pas de festivals pour le moment ]");
			PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.listeFestivals;");		
			try(ResultSet rs= ps.executeQuery()){
				
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | "+rs.getString(2));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage());
		}
	}
	public  int CompterFestivals() {		
		
		try {
			
			PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.compterFestivals();");
			
			try(ResultSet rs= ps.executeQuery()){
				int count = 0;
				while(rs.next()){
					count = rs.getInt(1);
				}
				return(count);
				
				
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()); 
		}
		return -1;
	}
}
