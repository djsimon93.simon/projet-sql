// @author Penelle Simon, Nguyen Huy
import java.util.Scanner;
import java.sql.*;



public class AppCentrale  {

	public static void main(String[] args){
		
		
		//Creation de la connexion
		MethodesAdmin m = new MethodesAdmin();
		
		
		//debut du main
		boolean continuer = true;
		
		System.out.println("bonjour et bienvenu sur l'application administrateur");
		while(continuer) {
			
			
			switch(m.afficherMenu()) {
			
			//ajouter salle
			case 1:
				try {
					
					m.ajouterSalle();
					
				}
				catch (IllegalArgumentException e) {
					e.getMessage();
				}
				
				break;
				
				
				
			//ajouter artistes
			case 2:
				try {
					
					
					m.ajouterArtiste();
				}
				catch (IllegalArgumentException e) {
					e.getMessage();
				}
				
				break;
				
			//ajout evenement
			case 3:
				try {
					
						
					m.ajouterEvenement();
				}
				catch (IllegalArgumentException e) {
					e.getMessage();
				}
				
				break;
				
			//ajout festival
			case 4:
				try {
					
					
					
					m.ajouterFestival();
				}
				catch (IllegalArgumentException e) {
					e.getMessage();
				}
				
				break;
				
			//ajouter concert a un evenement
			case 5:
				try {
					
					
					
					m.ajouterConcert();
					
					
					
				}
				catch (IllegalArgumentException e) {
					System.out.println(e.getMessage());
				}
				
				break;
				
			//visualiser artistes
			case 6:
				m.visualiserArtistes();
				
				break;
			//visualiser evenements et concerts entre 2 dates
			case 7:
				
				m.visualiserEventConcertsDate();
				
				break;
			//quitter
			case -1:
				continuer = false;
				System.out.println();
				System.out.println("Merci d'avoir  utilis� notre application!");
				System.out.println("revenez vite");
				break;
			}
		
		}
				
	}
	
}



