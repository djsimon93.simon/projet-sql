// @author Penelle Simon, Nguyen Huy
import java.util.Scanner;
import java.sql.*;



public class AppUtilisateurs {

	public static void main(String[] args){
		
		
		//Creation de la connexion
		MethodesUtilisateur m = new MethodesUtilisateur();
		int id_utilisateur = -1;


			
		//debut du main
		boolean continuer = true;
		boolean connecter = false;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bienvenue sur notre application d'�v�nements !");
		while(continuer) {
			
			System.out.println("Que voulez vous faire ?  ('-1' pour quitter)");
			System.out.println("1 - S'inscire");
			System.out.println("2 - Se connecter");
			
			
			String repons = scanner.nextLine();
			int reponse = Integer.parseInt(repons);
			switch(reponse) {
			
			//Inscription utilisateur
			case 1:
				m.ajouterUtilisateur();
				break;
				
			//Connexion utilisateur
			case 2:	
				id_utilisateur = m.verifierUtilisateur();
				if(id_utilisateur < 0) {
					System.out.println("echec connexion");
					id_utilisateur=-1;
					break;
				}
				System.out.println("vous etes connect�");
				connecter=true;
				
				
				while(connecter) {
					System.out.println("Que voulez vous faire ?  ('-1' pour vous deco)");
					System.out.println("1 - Voir evenements futurs");
					System.out.println("2 - Voir festivals futurs");
					System.out.println("3 - Voir ses reservations");
					
					String reponse2 = scanner.nextLine();
					int reponse2int = Integer.parseInt(reponse2);
					switch(reponse2int) {
					
					case 1:
						//FONCTION POUR AFFICHER EVENT FUTUR
						System.out.println("1 - Voir evenements d'une salle");
						System.out.println("2 - Voir evenements d'un artiste");
						
						
						String reponse3 = scanner.nextLine();
						int reponse3int = Integer.parseInt(reponse3);
						switch(reponse3int) {
						
						case 1:
							m.visualiserSalles();
							m.afficherEvenementFuturParSalle();
							m.reserverTickets(id_utilisateur);
							break;
							
						//verifier l'utili
						case 2:		
							m.visualiserArtistes();
							m.afficherEvenementFuturParArtiste();
							m.reserverTickets(id_utilisateur);
	
							break;
							
						}
						break;
					//verifier l'utili
					case 2:		
						m.afficherFestFutur();
						m.listeEventParFestival(id_utilisateur);
						break;
					case 3:		
						m.affichagerReservation(id_utilisateur);
						break;
						
					case -1:
						connecter = false;
						System.out.println();
						System.out.println("Deco reusssi!");
						System.out.println("revenez vite");
						break;
					}
				}
				
				break;
				
			case -1:
				continuer = false;
				System.out.println();
				System.out.println("Merci d'avoir  utilis� notre application!");
				System.out.println("revenez vite");
				break;
			}
		
		}
				
	}
	
}



