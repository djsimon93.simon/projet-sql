import java.util.Scanner;
import java.sql.*;

public class MethodesUtilisateur {

	Connection co;
	Scanner scanner;
	PreparedStatement psAjoutUtil;
	PreparedStatement psVerifUtil;
	PreparedStatement psAffichageEvenmtFutur;
	PreparedStatement psreservation;
	PreparedStatement psAffichageReservation;
	String sel;
	


	
	public MethodesUtilisateur() {

		 sel = BCrypt.gensalt();
		
		//se connecter a la db
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				System.out.println("Driver PostgreSQL manquant !");
				System.exit(1);
			}
			
			//String url = "jdbc:postgresql://localhost:5432/dbprojet";
			//String url = "jdbc:postgresql://localhost:5432/projet";
			String url="jdbc:postgresql://172.24.2.6:5432/dbsimonpenelle";
			Connection conn=null;
			try {
				conn=DriverManager.getConnection(url,"khacnguyen","F43T1HPJC");
				//conn=DriverManager.getConnection(url,"postgres","admin");
			} catch (SQLException e) {
				System.out.println("Impossible de joindre le server !");
				e.printStackTrace();
				System.exit(1);
			}
		this.co = conn;
		scanner = new Scanner(System.in);
		
	}
	
	/*methode pour ajouter un utilisateur
	 * @param nom de l'utils
	 * @param email
	 * @param mdp
	 */
	public void ajouterUtilisateur() {
		try {
			System.out.println("entrez votre nom");
			String nom = scanner.nextLine();
			System.out.println("entrez votre email");
			String email = scanner.nextLine();
			System.out.println("entrez votre mdp");
			String mdp = scanner.nextLine();
			String mdpAstocker = BCrypt.hashpw(mdp, sel);
			
			psAjoutUtil = co.prepareStatement("SELECT projet.inscriptionUtilisateur(?,?,?);");
			psAjoutUtil.setString(1,nom);
			psAjoutUtil.setString(2,email);
			psAjoutUtil.setString(3,mdpAstocker);
			psAjoutUtil.executeQuery();
		} 
		catch (SQLException se) {
			System.out.println(se.getMessage());
		
		}
		
	}

	public  int  verifierUtilisateur() {
		try {
			
			System.out.println("entrez votre email");
			String email = scanner.nextLine();
			System.out.println("entrez votre mdp");
			String mdp = scanner.nextLine();
			
			psVerifUtil = co.prepareStatement("SELECT mdp FROM projet.utilisateurs WHERE email=?;");
			psVerifUtil.setString(1,email);
			ResultSet rs = psVerifUtil.executeQuery();
			rs.next();
			String mdpStocke= rs.getString(1);
			if(BCrypt.checkpw(mdp, mdpStocke)) {
				psVerifUtil = co.prepareStatement("SELECT id_utilisateur FROM projet.utilisateurs WHERE email=?;");
				psVerifUtil.setString(1,email);
				rs = psVerifUtil.executeQuery();
				rs.next();
				return rs.getInt(1);
			}
		}
			
		catch (SQLException se) {
			System.out.println(se.getMessage());
		}
		return -1;
	}
	
	//methode pour visualiser les salles
		public  void visualiserSalles() {		
				
				try {
					
					PreparedStatement ps = co.prepareStatement("SELECT * FROM projet.listeSalles;");		
					try(ResultSet rs= ps.executeQuery()){
						
						while(rs.next()) {
							System.out.println(rs.getString(1)+" | "+rs.getString(2));
						}
					}
				} 
				catch (SQLException se) { 
					System.out.println(se.getMessage());
			
				}
		}
	
	public void afficherEvenementFuturParSalle() {
		try {
			System.out.println("Choississez la salle");
			String numSalle = scanner.nextLine();
			int numSalleint = Integer.parseInt(numSalle);
			
			psAffichageEvenmtFutur = co.prepareStatement("SELECT * FROM projet.listeEventFutur(?)"
					+ "t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);");
			psAffichageEvenmtFutur.setInt(1,numSalleint);
			
			try(ResultSet rs= psAffichageEvenmtFutur.executeQuery()){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | nom : "+rs.getString(2)+" | date evenement : "
							+rs.getString(3)+" | nom salle "+rs.getString(4)+" | prix "+rs.getString(5)+" | artistes "+rs.getString(6));
				}
			}
			
			
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()) ;
		}
	}
    public void afficherFestFutur() {		    	
    	try {
			Statement s = co.createStatement();
			
			try(ResultSet rs= s.executeQuery("SELECT * FROM projet.listeFestivalFutur;")){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | "+rs.getString(2)+" | Premier evenement : "
							+rs.getString(3)+" | Dernier evenement "+rs.getString(4) + " | Prix tot : " + rs.getString(5));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()) ;
		}
	}
    
    public void visualiserArtistes() {
		
		try {
			Statement s = co.createStatement();
			
			try(ResultSet rs= s.executeQuery("SELECT * FROM projet.listeArtiste;")){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | "+rs.getString(2)+" | "+rs.getString(3));
				}
			}
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()) ;
		}
	}
	
    public void afficherEvenementFuturParArtiste() {
		try {
			System.out.println("Choississez l'artiste");
			String numArt = scanner.nextLine();
			int numArtint = Integer.parseInt(numArt);
			
			psAffichageEvenmtFutur = co.prepareStatement("SELECT DISTINCT * FROM projet.listeEventFuturParArtiste(?)"
					+ "t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);");
			psAffichageEvenmtFutur.setInt(1,numArtint);
			
			try(ResultSet rs= psAffichageEvenmtFutur.executeQuery()){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | nom : "+rs.getString(2)+" | date evenement : "
							+rs.getString(3)+" | nom salle "+rs.getString(4)+" | prix "+rs.getString(5)+" | artistes "+rs.getString(6));
				}
			}
			
			
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()) ;
		}
	}
    
    public void reserverTickets(int numUtilisateur) {
    	try {
    	System.out.println("Quel event voulez vous reserver ?");
    	String numEvent = scanner.nextLine();
		int numEventint = Integer.parseInt(numEvent);
		System.out.println("Combien de tickets ? (MAX 4)");
    	String nbTicket = scanner.nextLine();
		int nbTicketint = Integer.parseInt(nbTicket);
		
		psreservation = co.prepareStatement("SELECT projet.reservation(?,?,?);");
		psreservation.setInt(1,numEventint);
		psreservation.setInt(2,numUtilisateur);
		psreservation.setInt(3,nbTicketint);
		psreservation.executeQuery();
    	}
    	catch(SQLException se) { 
    		System.out.println(se.getMessage()) ;
		}
		
    }
    
    public void listeEventParFestival(int numUtilisateur) {

    	try {
			System.out.println("Choississez le festival");
			String numFest = scanner.nextLine();
			int numFestInt = Integer.parseInt(numFest);
			
			psAffichageEvenmtFutur = co.prepareStatement("SELECT DISTINCT * FROM projet.listeEventParFestival(?)\r\n" + 
						"t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);");
			psAffichageEvenmtFutur.setInt(1,numFestInt);
			
			try(ResultSet rs= psAffichageEvenmtFutur.executeQuery()){
				while(rs.next()) {
					System.out.println(rs.getString(1)+" | nom : "+rs.getString(2)+" | date evenement : "
							+rs.getString(3)+" | nom salle "+rs.getString(4)+" | prix : "+rs.getString(5)+" | artistes "+rs.getString(6));
				}
			}
			
			System.out.println("Vpulez vous reservez tout les evnt de ce festivals ? (o/n)");
			String reponse = scanner.nextLine();
			char repChar = reponse.charAt(0);
			if(repChar=='o') {
				System.out.println("Combien de tickets ? (MAX 4)");
		    	String nbTicket = scanner.nextLine();
				int nbTicketint = Integer.parseInt(nbTicket);
				psreservation = co.prepareStatement("SELECT projet.reservationToutparFest(?,?,?);");
				psreservation.setInt(1,numFestInt);
				psreservation.setInt(2,numUtilisateur);
				psreservation.setInt(3,nbTicketint);
				psreservation.executeQuery();
			}
			
		} 
		catch (SQLException se) { 
			System.out.println(se.getMessage()) ;
		}
	}

	public void affichagerReservation(int id_utilisateur) {
		// TODO Auto-generated method stub
		try {
			psAffichageReservation = co.prepareStatement("SELECT * FROM projet.listeReservations WHERE  id_utilisateur=?;");
			psAffichageReservation.setInt(1,id_utilisateur);
			try(ResultSet rs= psAffichageReservation.executeQuery()){
				while(rs.next()) {
					System.out.println("Nom evenement : " +rs.getString(1)+" | Date : "+rs.getString(2)+" | Salle : "
							+rs.getString(3)+" | N� reservation : "+rs.getString(4)+" | Tickets achet� : "+rs.getString(5));
				}
			}
	
		}
			
		catch (SQLException se) {
			System.out.println(se.getMessage());
		}
	
	}
    
}
