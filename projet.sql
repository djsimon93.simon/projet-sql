DROP SCHEMA IF EXISTS projet CASCADE;
CREATE SCHEMA projet



--Creation de la db

CREATE TABLE projet.artistes (
id_artiste SERIAL PRIMARY KEY,
nom VARCHAR (50) NOT NULL CHECK (nom != ''),
nationalite VARCHAR (50) NULL, 
nb_ticket INTEGER NOT NULL
);

CREATE TABLE projet.salles(
id_salle SERIAL PRIMARY KEY,
nom VARCHAR(50) NOT NULL CHECK (nom != ''),
ville VARCHAR(50) NOT NULL,
capacite INTEGER NOT NULL CHECK (capacite>=0)
);

CREATE TABLE projet.festivals(
id_festival SERIAL PRIMARY KEY,
nom VARCHAR(50)
);

CREATE TABLE projet.evenements(
id_evenement SERIAL PRIMARY KEY,
nom VARCHAR(50) NOT NULL CHECK (nom != ''),
date_evenement DATE NOT NULL ,
prix INTEGER NOT NULL,
id_salle INTEGER NOT NULL
	REFERENCES projet.salles(id_salle),
id_festival INTEGER NULL
	REFERENCES projet.festivals(id_festival),
UNIQUE (id_salle,date_evenement)
);

CREATE TABLE projet.concerts(
id_concert SERIAL PRIMARY KEY,
horaire TIME NOT NULL,
id_artiste INTEGER NOT NULL
	REFERENCES projet.artistes(id_artiste),
id_evenement INTEGER NOT NULL
	REFERENCES projet.evenements(id_evenement),
UNIQUE (id_evenement,id_artiste)
);

CREATE TABLE projet.utilisateurs(
id_utilisateur SERIAL PRIMARY KEY,
nom VARCHAR(50) NOT NULL CHECK (nom != ''),
email VARCHAR(50) NOT NULL,
mdp VARCHAR(200) NOT NULL
);

CREATE TABLE projet.reservations(
num_res INTEGER NOT NULL,
id_evenement INTEGER NOT NULL
	REFERENCES projet.evenements(id_evenement),
id_utilisateur INTEGER NOT NULL
	REFERENCES  projet.utilisateurs(id_utilisateur),
nbr_ticket INTEGER NOT NULL CHECK (nbr_ticket < 5 AND nbr_ticket > 0),
PRIMARY KEY (num_res,id_evenement)
);

--ajouter un artiste

CREATE OR REPLACE FUNCTION projet.ajoutArtiste(nom VARCHAR(50), nationalite VARCHAR(50)) 
				  RETURNS INTEGER AS $$
BEGIN
INSERT INTO projet.artistes VALUES (DEFAULT,nom,nationalite,0);
RETURN 0;
END;
$$ LANGUAgE plpgsql;

--ajouter une salle

CREATE OR REPLACE FUNCTION projet.ajoutSalle(nom VARCHAR(50), ville VARCHAR(50), capacite INTEGER) 
				  RETURNS INTEGER AS $$
BEGIN
INSERT INTO projet.salles VALUES (DEFAULT,nom,ville,capacite);
RETURN 1;
END;
$$ LANGUAgE plpgsql;

--ajouter un evenement

CREATE OR REPLACE FUNCTION projet.ajoutEvenmt(nom VARCHAR(50), date_evenement DATE, id_salle INTEGER, prix INTEGER, id_festival INTEGER) 
				  RETURNS INTEGER AS $$
BEGIN
IF (date_evenement < NOW()) THEN RAISE data_exception;
END IF;
IF (id_festival = -1) THEN id_festival = NULL;
END IF;
INSERT INTO projet.evenements VALUES (DEFAULT,nom,date_evenement,prix,id_salle,id_festival);
RETURN 0;
END;
$$ LANGUAgE plpgsql;

--ajouter un festival

CREATE OR REPLACE FUNCTION projet.ajoutFestival(nom VARCHAR(50)) 
				  RETURNS INTEGER AS $$
BEGIN
INSERT INTO projet.festivals VALUES (DEFAULT,nom);
RETURN 0;
END;
$$ LANGUAgE plpgsql;

--ajouter un concert 

CREATE OR REPLACE FUNCTION projet.ajoutConcert(horaire TIME, id_artiste INTEGER, id_evenmt INTEGER) 
				  RETURNS INTEGER AS $$
BEGIN
INSERT INTO projet.concerts VALUES (DEFAULT,horaire,id_artiste,id_evenmt);
RETURN 0;
END;
$$ LANGUAgE plpgsql;

--Liste artiste par ticket vendu

CREATE VIEW projet.listeArtiste as
	SELECT a.id_artiste, a.nom as "nom artiste",a.nationalite, a.nb_ticket
	FROM projet.artistes a
	ORDER BY nb_ticket desc;

SELECT * FROM projet.listeArtiste;

--Liste des salles
CREATE VIEW projet.listeSalles as
	SELECT s.id_salle, s.nom as nom_salle
	FROM  projet.salles s;
	
	
SELECT * FROM projet.listeSalles;

--Liste des festivals
CREATE VIEW projet.listeFestivals as
	SELECT f.id_festival, f.nom as nom_festival
	FROM  projet.festivals f;
	
	
SELECT * FROM projet.listeFestivals;


--Fonction pour evnt entre 2 date

CREATE OR REPLACE FUNCTION projet.listeEvenmtDate(date1 DATE, date2 DATE) RETURNS SETOF RECORD AS $$
DECLARE
	dateMin DATE;
	dateMAx DATE;
	sortie RECORD;
	concert RECORD;
BEGIN
dateMin:=date1; dateMax:=date2;

FOR concert IN SELECT  c.id_concert , e.nom, e.date_evenement, e.id_salle, e.id_festival 
			FROM projet.evenements e, projet.concerts c
			WHERE e.id_evenement=c.id_evenement AND e.date_evenement >= dateMin AND e.date_evenement <=dateMax
			LOOP
			SELECT  concert.id_concert, concert.nom, concert.date_evenement,concert.id_salle, concert.id_festival INTO sortie;
			RETURN NEXT sortie;
END LOOP;

RETURN;
END;
$$ LANGUAGE 'plpgsql';
			
SELECT * FROM projet.listeEvenmtDate('2020-05-10','2020-05-15')
t(id_concert INTEGER, nom_event VARCHAR(50),date_event DATE, id_salle INTEGER,id_festival INTEGER);


CREATE OR REPLACE VIEW projet.listeEvenementss as
	SELECT   e.nom, e.date_evenement, s.nom as "nom_salle", f.nom as "nom_fest", sum(r.nbr_ticket)
			FROM projet.evenements e 
			LEFT OUTER JOIN projet.festivals f ON  f.id_festival=e.id_festival
			LEFT OUTER JOIN projet.reservations r ON r.id_evenement=e.id_evenement,projet.salles s
			WHERE s.id_salle=e.id_salle
			
			GROUP BY e.nom, e.date_evenement, nom_salle, nom_fest;
			

SELECT * FROM projet.listeEvenementss WHERE date_evenement >= '2020-05-10' AND date_evenement <='2020-05-15';


CREATE OR REPLACE VIEW projet.listeEvenements as
	SELECT  e.id_evenement, e.nom, e.date_evenement, s.nom as "nom_salle"
			FROM projet.evenements e, projet.salles s
			WHERE e.id_salle= s.id_salle
			ORDER BY e.date_evenement;


	
--Inscription user 

CREATE OR REPLACE function projet.inscriptionUtilisateur(nom VARCHAR(50),email VARCHAR(50), mdp VARCHAR(200))
	RETURNS INTEGER as $$
DECLARE
BEGIN
INSERT INTO projet.utilisateurs VALUES (DEFAULT,nom, email,mdp);
RETURN 0;
END;
$$ LANGUAGE 'plpgsql';

--Verification connexion 

CREATE OR REPLACE function projet.connexionUtilisateur(emailC VARCHAR(50), mdpC VARCHAR(50))
	RETURNS INTEGER as $$
DECLARE
num_utilisateur INTEGER;
BEGIN
IF NOT EXISTS (SELECT * FROM projet.utilisateurs WHERE email=emailC)
	THEN RAISE 'mauvais email';
END IF;
IF NOT EXISTS (SELECT * FROM projet.utilisateurs WHERE email=email AND mdp=mdpC)
	THEN RAISE 'mauvais mdp';
END IF;
SELECT id_utilisateur FROM projet.utilisateurs WHERE email=email AND mdp=mdpC INTO num_utilisateur;
RETURN num_utilisateur ;
END;
$$ LANGUAGE 'plpgsql';

--Compter les festivals
CREATE OR REPLACE function projet.compterfestivals()
	RETURNS INTEGER as $$
DECLARE
BEGIN
RETURN (SELECT COUNT(*) FROM projet.festivals);
END;
$$ language 'plpgsql';

--Trigger pour ajout concert dans un evenmt (mettre ticket vendu artiste a jour et verifier date concert pas identique)

CREATE OR REPLACE FUNCTION projet.ajoutConcertTrigger() RETURNS TRIGGER AS $$
DECLARE
tot_ticket_vendu INTEGER;
date_concert DATE;
BEGIN
--Ajout du nbre de ticket vendu a m'artiste
	SELECT sum(r.nbr_ticket) 
	FROM projet.reservations r 
	WHERE NEW.id_evenement = r.id_evenement INTO tot_ticket_vendu;
	
	IF(tot_ticket_vendu IS null ) 
		THEN tot_ticket_vendu:=0;
	END IF;
	
	UPDATE projet.artistes SET nb_ticket = nb_ticket+tot_ticket_vendu WHERE id_artiste=NEW.id_artiste;

--Verifier si l'artiste a deja un concert ce jour
	SELECT e.date_evenement 
	FROM projet.evenements e
	WHERE e.id_evenement = NEW.id_evenement INTO date_concert;
	IF date_concert IN (SELECT e.date_evenement 
						FROM projet.evenements e,projet.concerts c, projet.artistes a 
						WHERE e.id_evenement = c.id_evenement AND c.id_artiste = a.id_artiste 
						AND a.id_artiste = NEW.id_artiste)
		THEN raise 'artiste a déja un concert ce jour la';
	END IF;
	
--Verifier si un concert a deja lieu a cet heure
	IF NEW.horaire IN (SELECT c.horaire 
						FROM projet.evenements e,projet.concerts c, projet.artistes a 
						WHERE e.id_evenement = c.id_evenement AND NEW.id_evenement=c.id_evenement)
		THEN raise 'il y a déja un concert a cette heure la';
	END IF;

RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER ajout_concert_trigger BEFORE INSERT ON projet.concerts FOR EACH ROW
EXECUTE PROCEDURE projet.ajoutConcertTrigger();






--visualiser evenement futur d'une salle trié par date

CREATE OR REPLACE function projet.listeEventFutur(num_salle INTEGER)
	RETURNS SETOF RECORD AS $$
DECLARE
evenements RECORD;
sortie RECORD;
artiste RECORD;
sep VARCHAR;
texte VARCHAR;
	
BEGIN
FOR evenements IN SELECT e.id_evenement, e.nom,e.date_evenement, s.nom as "nom_salle",e.prix
			FROM projet.salles s , projet.evenements e  
			WHERE s.id_salle=e.id_salle and e.date_evenement > now() and num_salle=e.id_salle
			ORDER BY e.date_evenement  LOOP
			texte:='';sep:='';
				FOR artiste IN SELECT a.nom
								FROM projet.artistes a, projet.concerts c, projet.evenements e
								WHERE a.id_artiste = c.id_artiste and e.id_evenement = c.id_evenement and e.id_salle = num_salle LOOP
								texte:=texte || sep || artiste.nom;
								sep:= ' + ';
								END LOOP;
			SELECT evenements.id_evenement, evenements.nom,evenements.date_evenement, evenements.nom_salle, evenements.prix ,texte INTO sortie;
			RETURN NEXT sortie;
END LOOP;
RETURN;
END;
$$ language plpgsql;
SELECT * FROM projet.listeEventFutur(2)
t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);


--visualiser evenement futur en fct d'un artiste trié par date

CREATE OR REPLACE function projet.listeEventFuturParArtiste(num_artiste INTEGER)
	RETURNS SETOF RECORD AS $$
DECLARE
evenements RECORD;
sortie RECORD;
artiste RECORD;
sep VARCHAR;
texte VARCHAR;
	
BEGIN
FOR evenements IN SELECT e.id_evenement, e.nom,e.date_evenement, s.nom as "nom_salle",e.prix
			FROM projet.salles s , projet.evenements e , projet.artistes a,projet.concerts c
			WHERE s.id_salle=e.id_salle and e.date_evenement > now() and num_artiste=c.id_artiste 
			and c.id_evenement=e.id_evenement
			ORDER BY e.date_evenement  LOOP
			texte:='';sep:='';
				FOR artiste IN SELECT a.nom 
								FROM projet.concerts c, projet.artistes a
								WHERE evenements.id_evenement = c.id_evenement and a.id_artiste=c.id_artiste
								LOOP
								texte:=texte || sep || artiste.nom;
								sep:= ' + ';
								END LOOP;
			SELECT evenements.id_evenement, evenements.nom,evenements.date_evenement, evenements.nom_salle, evenements.prix ,texte INTO sortie;
			RETURN NEXT sortie;
END LOOP;
RETURN;
END;
$$ language plpgsql;

SELECT DISTINCT * FROM projet.listeEventFuturParArtiste(2)
t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);

--visualiser festival futur

CREATE OR REPLACE VIEW projet.listeFestivalFutur as
	SELECT f.id_festival, f.nom, min(e.date_evenement)as "premier_event",max(e.date_evenement)as "dernier_event",
		sum( e.prix)
	FROM projet.festivals f,projet.evenements e  
	WHERE f.id_festival=e.id_festival
		and e.date_evenement > now()
	GROUP by f.id_festival
	ORDER BY min(e.date_evenement);


select * from projet.evenements;

SELECT * FROM projet.compterFestivals();

SELECT * FROM projet.listeFestivalFutur;

--visualiser events d'un festival

CREATE OR REPLACE FUNCTION projet.listeEventParFestival(num_festival INTEGER) 
	RETURNS SETOF RECORD AS $$
DECLARE
evenements RECORD;
sortie RECORD;
artiste RECORD;
sep VARCHAR;
texte VARCHAR;

BEGIN
FOR evenements IN SELECT e.id_evenement, e.nom as "nom_event", e.date_evenement, s.nom as "nom_salle", e.prix 
			FROM projet.evenements e, projet.salles s
			WHERE e.id_salle=s.id_salle AND e.id_festival = num_festival 
			ORDER BY e.date_evenement LOOP
			texte:='';sep:='';
			FOR artiste IN SELECT a.nom 
								FROM projet.concerts c, projet.artistes a
								WHERE evenements.id_evenement = c.id_evenement and a.id_artiste=c.id_artiste
								LOOP
								texte:=texte || sep || artiste.nom;
								sep:= ' + ';
								END LOOP;
			
			SELECT evenements.id_evenement, evenements.nom_event,evenements.date_evenement, evenements.nom_salle, evenements.prix,texte INTO sortie;
			RETURN NEXT sortie;
END LOOP;
RETURN;
END;
$$ language plpgsql;

SELECT DISTINCT * FROM projet.listeEventParFestival(2)
t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR);

--reserver un ticket pour un event 
--@param: nombre de tickets
CREATE OR REPLACE FUNCTION projet.reservation(num_evenement INTEGER,  num_utilisateur INTEGER, nb_ticket INTEGER) 
	RETURNS BOOLEAN AS $$
DECLARE
num_res INTEGER;
BEGIN

num_res := (SELECT count(r.*)+1 FROM projet.reservations r WHERE r.id_evenement=num_evenement);

INSERT INTO projet.reservations VALUES (num_res,num_evenement,num_utilisateur,nb_ticket);
RETURN true;
END;
$$ language plpgsql;





--TRIGGER reservations

CREATE OR REPLACE FUNCTION projet.reservationTrigger() RETURNS TRIGGER AS $$
DECLARE
nb_ticket_achete INTEGER;
date_evemnt DATE;
capacite_salle INTEGER;
nbr_ticket_tot INTEGER;
nbr_concert_evenmt INTEGER;
artiste RECORD;

BEGIN
--Lorsque le utilisateur a déjà réservé des tickets pour un autre événement se déroulant à la même date.
	SELECT DISTINCT e.date_evenement 
	FROM projet.evenements e
	WHERE e.id_evenement = NEW.id_evenement INTO date_evemnt;
	
	IF(EXISTS (SELECT r.* 
			   FROM projet.reservations r, projet.evenements e
			   WHERE r.id_utilisateur=NEW.id_utilisateur AND r.id_evenement!=NEW.id_evenement 
			   AND date_evemnt=e.date_evenement AND e.id_evenement=r.id_evenement) )
	THEN raise 'Deja reserve des tickets pour un evnt le meme jour';
	END IF;

--Verif le nombre de ticket ne peut pas depasser 4 par pers/par evnmt
	SELECT sum(r.nbr_ticket)
	FROM projet.reservations r 
	WHERE NEW.id_utilisateur = r.id_utilisateur AND NEW.id_evenement=r.id_evenement INTO nb_ticket_achete;
	
	IF(nb_ticket_achete+NEW.nbr_ticket > 4 ) 
		THEN raise 'Nbre maximun de ticket achete depasse (max 4)';
	END IF;
	
--Verifier la capacite de la salle
	SELECT s.capacite 
	FROM projet.salles s,projet.evenements e 
	WHERE s.id_salle=e.id_salle AND e.id_evenement=NEW.id_evenement INTO capacite_salle;
	
	SELECT sum(r.nbr_ticket)
	FROM projet.reservations r 
	WHERE r.id_evenement = NEW.id_evenement INTO nbr_ticket_tot;
	
	if(nbr_ticket_tot is null) then nbr_ticket_tot:=0; end if;
	
	IF (capacite_salle < nbr_ticket_tot+NEW.nbr_ticket )
		THEN raise 'Plus assez de place dans la salle';
	END IF;

--Si l'evenmt est deja passe
	IF(date_evemnt<now())
		THEN raise 'Evenement deja passe';
	END IF;
	
--Lorsque l’événement ne contient pas encore de concert (événement pas finalisé)
	SELECT count(c.id_evenement)
	FROM projet.concerts c 
	WHERE c.id_evenement = NEW.id_evenement INTO nbr_concert_evenmt;
	
	IF (nbr_concert_evenmt = 0)
		THEN raise 'Pas de concert dans cet evenement';
	END IF;
--Ajout des tickets achete a l'artiste
	FOR artiste IN SELECT a.*
					FROM projet.artistes a, projet.concerts c
					WHERE a.id_artiste = c.id_artiste AND c.id_evenement=NEW.id_evenement LOOP
		UPDATE projet.artistes SET nb_ticket = nb_ticket+NEW.nbr_ticket WHERE id_artiste=artiste.id_artiste;
	END LOOP;

	
RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TRIGGER reservation_trigger BEFORE INSERT ON projet.reservations FOR EACH ROW
EXECUTE PROCEDURE projet.reservationTrigger();

--reserver tout les evnt d'un festival 

CREATE OR REPLACE FUNCTION projet.reservationToutparFest(num_fest INTEGER, num_utilisateur INTEGER, nb_ticket INTEGER) 
	RETURNS BOOLEAN AS $$
DECLARE
evenmtFest RECORD;
res BOOLEAN;
BEGIN
	FOR evenmtFest IN SELECT DISTINCT id_evenement
					  FROM projet.listeEventParFestival(num_fest)
					  t(id_evenement INTEGER,nom VARCHAR(50),date_evenement DATE,nom_salle VARCHAR(50),prix INTEGER,artistes VARCHAR) LOOP
		SELECT projet.reservation(evenmtFest.id_evenement,num_utilisateur,nb_ticket) INTO res;
	END LOOP;
RETURN res;
END;
$$ language plpgsql;

--Afficher les reservations d'un utilisateur
CREATE OR REPLACE FUNCTION projet.listeResUtilisateur (num_util INTEGER)
	RETURNS SETOF RECORD AS $$
DECLARE 
res RECORD;
sortie RECORD;
BEGIN
	FOR res IN SELECT e.nom,e.date_evenement,s.nom as "nom_salle", r.num_res, r.nbr_ticket
				  FROM projet.evenements e, projet.reservations r,projet.salles s
				  WHERE e.id_evenement=r.id_evenement AND r.id_utilisateur=num_util AND s.id_salle=e.id_salle LOOP
			SELECT res.nom,res.date_evenement,res.nom_salle,res.num_res,res.nbr_ticket  INTO sortie;
			RETURN NEXT sortie;
	END LOOP;
RETURN;
END;
$$ language plpgsql;

CREATE OR REPLACE VIEW projet.listeReservations as
	SELECT e.nom,e.date_evenement,s.nom as "nom_salle", r.num_res, r.nbr_ticket, r.id_utilisateur
	FROM projet.evenements e, projet.reservations r,projet.salles s
	WHERE e.id_evenement=r.id_evenement AND s.id_salle=e.id_salle;
	
SELECT * FROM projet.listeReservations WHERE  id_utilisateur=2;



--DEMO


SELECT projet.ajoutArtiste('Eminem', 'Belge');
SELECT projet.ajoutArtiste('Byeonce', 'Viet');

SELECT projet.ajoutSalle('Palais 12', 'bx', 3);

SELECT projet.ajoutFestival('UCL');

--TEST




--grants
GRANT CONNECT ON DATABASE dbsimonpenelle TO khacnguyen;
GRANT USAGE ON SCHEMA projet TO khacnguyen;
GRANT SELECT ON ALL TABLES IN SCHEMA projet TO khacnguyen;
GRANT UPDATE,INSERT ON projet.artistes,projet.reservations,projet.utilisateurs TO khacnguyen;
GRANT USAGE,SELECT ON sequence projet.utilisateurs_id_utilisateur_seq to khacnguyen;



